﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class move : MonoBehaviour
{
Rigidbody2D body;
public Text puntaje;
public int count = 20;
float horizontal;
float vertical;

public float runSpeed;

void Start ()
{
   body = GetComponent<Rigidbody2D>(); 
}
private void OnTriggerEnter2D(Collider2D collision)
{
    if (collision.tag == "enemigo")
    {
        count--;
        puntaje.text = count.ToString();  
    }
}
void Update ()
{
   horizontal = Input.GetAxisRaw("Horizontal");
   vertical = Input.GetAxisRaw("Vertical"); 
}

private void FixedUpdate()
{  
   body.velocity = new Vector2(horizontal * runSpeed, vertical * runSpeed);
}
}